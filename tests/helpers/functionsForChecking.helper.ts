import { expect } from 'chai';
import path from 'path';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 5000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(maxResponseTime);
}


export function checkResponseSchemas(response, schema: string) {
    expect(response.schema, `Response schema should be ${schema}`).to.be.jsonSchema(schema)
}

