import { tokenToString } from "typescript";
import { ApiRequest } from "../request";
const baseUrl: string = global.appConfig.baseUrl;

export class TasqueController {
    async userRegistration(userData: object) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('POST')
        .body(userData)
        .url(`Register`)
        .send();
    return response;
    }

   async allUsersInformation() {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('GET')
        .url(`Users`)
        .send();
    return response;
    }

    async usersAuthorization(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('POST')
        .url(`Auth/login`)
        .body({
            email: emailValue,
            password: passwordValue,
        })
        .send();
    return response;
    }

    async usersInfoByToken(token: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('GET')
        .url(`Users/fromToken`)
        .bearerToken(token)
        .send();
    return response;
    }

    async updateUser(token: string, id: number, avatar: string, email: string, userName: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('PUT')
        .url(`Users`)
        .body({
            id: id,
            email: email,
            avatar: avatar,
            userName: userName
        })
        .bearerToken(token)
        .send();
    return response;
    }


    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`Users/${id}`)
            .send();
        return response;
    }

    async deleteUser(id, token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`Users/${id}`)
            .bearerToken(token)
            .send();
        return response;
    }

}
