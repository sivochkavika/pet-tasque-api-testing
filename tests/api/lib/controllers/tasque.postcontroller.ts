import { tokenToString } from "typescript";
import { ApiRequest } from "../request";
const baseUrl: string = global.appConfig.baseUrl;

export class TasqueController {
    async userRegistration(userData: object) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('POST')
        .body(userData)
        .url(`Register`)
        .send();
    return response;
    }


    async createPost(token: string, authorId: number, previewImage: string, postBody: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('POST')
        .url(`Posts`)
        .body({
            authorId: authorId,
            previewImage: previewImage,
            body: postBody
        })
        .bearerToken(token)
        .send();
    return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('GET')
        .url(`Posts`)
        .send();
    return response;
    }

   async addLikeReaction(token: string, idPost: number, isLike: boolean, userId: number) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('POST')
        .url(`Posts/like`)
        .bearerToken(token)
        .body({
            entityId: idPost,
            isLike: isLike,
            userId: userId
        })
        .send();
    return response;
    }


    async Addcomment(token: string, userId: number, idPost: number, commentBody: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`Comments`)
            .bearerToken(token)
            .body({
                authorId: userId,
                postId: idPost,
                body: commentBody
            })
            .send();
        return response;
    }

  
    async updateUser(token: string, id: number, avatar: string, email: string, userName: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method('PUT')
        .url(`Users`)
        .body({
            id: id,
            avatar: avatar,
            email: email,
            userName: userName
        })
        .bearerToken(token)
        .send();
    return response;
    }

    async deleteUser(id, token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`Users/${id}`)
            .bearerToken(token)
            .send();
        return response;
    }

 }
