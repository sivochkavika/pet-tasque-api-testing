import { expect } from "chai";  
import { TasqueController } from "../lib/controllers/tasque.controller";
import { checkStatusCode, checkResponseTime, checkResponseSchemas } from "../../helpers/functionsForChecking.helper";
const tasque = new TasqueController();
const schemas = require('./data/schemas_reg_auth.json');
const schemas1 = require('./data/schemas_user_by_id.json');
const schemas2 = require('./data/schemas_all_users.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));
const randomEmail = require('random-email');

describe("Tasque controller", () => {
    let userId: number;
    let token: string;
    let refreshToken: string;
    let email = randomEmail();
    let newEmail = randomEmail();
    let invalidToken: string;

    type User = {
        avatar: string;
        email: string;
        userName: string;
        password: string;
      };

    var userData: User = {
        avatar: "photo",
        email: email,
        userName: "vicki",
        password: "12345678"
      };

    var newUserData: User = {
        avatar: "photo",
        email: newEmail,
        userName: "SVV",
        password: "12345678"
      };

      let invalidUserData = [
        {avatar: 'photo', email: 'vickigmail.com', userName:'Viktoriya', password: '12345678'},
        // {avatar: 'photo', email: 'vicki@gmailcom', userName:'Viktoriya', password: '12345678'},
        {avatar: 'photo', email: '', userName:'Viktoriya', password: '12345678'},
        {avatar: 'photo', email: '@gmail.com', userName:'Viktoriya', password: '12345678'},
        {avatar: 'photo', email: 'vicki@', userName:'Viktoriya', password: '12345678'},
      ];

  
      invalidToken = "123";
      let userIdNotExist = 123456;
      let expectedErrorMessage = `Entity User with id (${userIdNotExist}) was not found.`;


    it(`register on tasque`, async () => {

        let response = await tasque.userRegistration(userData);
       
      
        checkStatusCode(response, 201);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas);

        
        expect(response.body.user.userName).to.be.equal(userData.userName);
        expect(response.body.user.email).to.be.equal(userData.email);
        
        userId = response.body.user.id;
        token = response.body.token.accessToken.token;
        refreshToken = response.body.token.refreshToken;
       
     });

     it(`all users information`, async () => {

        let response = await tasque.allUsersInformation();

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas2);

        
        var createdUser = response.body.find(o => o.id === userId);
  
        expect(createdUser.id).to.be.equal(userId);
        expect(createdUser.userName).to.be.equal(userData.userName);
        expect(createdUser.email).to.be.equal(userData.email);
        
     });

     it(`auth user`, async () => {

        let response = await tasque.usersAuthorization(userData.email, userData.password);
     
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas);
        
        expect(response.body.user.id).to.be.equal(userId);
        expect(response.body.user.userName).to.be.equal(userData.userName);
        expect(response.body.user.email).to.be.equal(userData.email);
        
        token = response.body.token.accessToken.token;
        refreshToken = response.body.token.refreshToken;
      
     });


     it(`user info by token`, async () => {

        let response = await tasque.usersInfoByToken(token);
 
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas1);
           
        expect(response.body.id).to.be.equal(userId);
        expect(response.body.userName).to.be.equal(userData.userName);
        expect(response.body.email).to.be.equal(userData.email);
        
      });


      it(`update user`, async () => {

        let response = await tasque.updateUser(token, userId, newUserData.avatar, newUserData.email, newUserData.userName);
       
        checkStatusCode(response, 204);
        checkResponseTime(response, 5000);
         
      });


      it(`user info by token after update`, async () => {

        let response = await tasque.usersInfoByToken(token);
  
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas1);

  
        expect(response.body.id).to.be.equal(userId);
        expect(response.body.userName).to.be.equal(newUserData.userName);
        expect(response.body.email).to.be.equal(newUserData.email);
        
      });

      it(`user info by id`, async () => {

        let response = await tasque.getUserById(userId);
  
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas1);
 
        expect(response.body.id).to.be.equal(userId);
        expect(response.body.userName).to.be.equal(newUserData.userName);
        expect(response.body.email).to.be.equal(newUserData.email);
        
      });


      it(`delete user`, async () => {

        let response = await tasque.deleteUser(userId, token);
   
        checkStatusCode(response, 204);
        checkResponseTime(response, 5000);
                
      });

      
        
      invalidUserData.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.avatar} ${credentials.email} ${credentials.userName} ${credentials.password}'`, async () => {
            let response = await tasque.userRegistration(credentials);

            checkStatusCode(response, 400); 
            checkResponseTime(response, 5000);
            expect(response.body.message).to.be.equal("Validation errors");
            expect(response.body.errors[0]).to.be.equal("'Email' is not a valid email address.");
        });
    });



     it(`delete user with invalid token`, async () => {

      let response = await tasque.deleteUser(userId, invalidToken);
     
      checkStatusCode(response, 401);
      checkResponseTime(response, 5000);
                    
    });

    it(`update not exist user`, async () => {

      let response = await tasque.updateUser(token, userIdNotExist, newUserData.avatar, newUserData.email, newUserData.userName);
          
      checkStatusCode(response, 404);
      checkResponseTime(response, 5000);
     
      expect(response.body.error).to.be.equal(expectedErrorMessage);
              
    });

 
})