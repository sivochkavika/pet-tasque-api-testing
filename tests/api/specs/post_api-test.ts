import { expect } from "chai";  
import { TasqueController } from "../lib/controllers/tasque.postcontroller";
import { checkStatusCode, checkResponseTime, checkResponseSchemas } from "../../helpers/functionsForChecking.helper";
const tasque = new TasqueController();
const schemas = require('./data/schemas_reg_auth.json');
const schemas3 = require('./data/schemas_create_post.json');
const schemas4 = require('./data/schemas_all_posts.json');
const schemas5 = require('./data/schemas_add_comment.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));
const randomEmail = require('random-email');

describe("Tasque controller", () => {
    let userId: number;
    let token: string;
    let refreshToken: string;
    let email = randomEmail();
    let newEmail = randomEmail();
    let idPost: number;
    let previewImage: string;
    let postBody: string;
    let isLike: boolean;
    let commentBody: string;
    let idComment: string

     type User = {
        avatar: string;
        email: string;
        userName: string;
        password: string;
      };

    type Post = {
        authorId: number;
        previewImage: string; 
        postBody: string;
      };

    var userData: User = {
        avatar: "photo",
        email: email,
        userName: "vicki",
        password: "12345678"
      };

    isLike = true;
    commentBody = "It is very interesting";

  
    var newUserData: User = {
        avatar: "photo",
        email: newEmail,
        userName: "SVV",
        password: "12345678"
      };

    before('should be registration', async () => {
      let response = await tasque.userRegistration(userData);

      checkStatusCode(response, 201);
      checkResponseTime(response, 5000);
      checkResponseSchemas(response, schemas);

      expect(response.body.user.userName).to.be.equal(userData.userName);
      expect(response.body.user.email).to.be.equal(userData.email);

      userId = response.body.user.id;
      token = response.body.token.accessToken.token;
      refreshToken = response.body.token.refreshToken;

    })

   
      it(`create post`, async () => {
        var postData: Post = {
          authorId: userId,
          previewImage: "picture",
          postBody: "forest"        
        };

        let response = await tasque.createPost(token, userId, postData.previewImage, postData.postBody);
      
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas3);
        
        expect(response.body.author.id).to.be.equal(userId);
        expect(response.body.author.userName).to.be.equal(userData.userName);
        expect(response.body.author.email).to.be.equal(userData.email);
        expect(response.body.previewImage).to.be.equal(postData.previewImage);
        expect(response.body.body).to.be.equal(postData.postBody);

        idPost = response.body.id;
        previewImage = response.body.previewImage;
        postBody = response.body.body;
        
      });

      it(`all posts`, async () => {

        let response = await tasque.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas4);

        var createdPost = response.body.find(o => o.id === idPost);
     
        expect(createdPost.id).to.be.equal(idPost);
        expect(createdPost.previewImage).to.be.equal(previewImage);
        expect(createdPost.body).to.be.equal(postBody);
        expect(createdPost.author.id).to.be.equal(userId);
        expect(createdPost.author.userName).to.be.equal(userData.userName);
        expect(createdPost.author.email).to.be.equal(userData.email);
        
     });
 
     it(`add like reaction`, async () => {
      

      let response = await tasque.addLikeReaction(token, idPost, isLike, userId);
    
      checkStatusCode(response, 200);
      checkResponseTime(response, 5000);
   
    });

    it(`add comment`, async () => {
      

      let response = await tasque.Addcomment(token, userId, idPost, commentBody);
         
      idComment = response.body.id;

      checkStatusCode(response, 200);
      checkResponseTime(response, 5000);
      checkResponseSchemas(response, schemas5);
      
      expect(response.body.author.id).to.be.equal(userId);
      expect(response.body.author.userName).to.be.equal(userData.userName);
      expect(response.body.author.email).to.be.equal(userData.email);
      expect(response.body.id).to.be.equal(idComment);
      expect(response.body.body).to.be.equal(commentBody);

              
    });

    it(`check like and comment`, async () => {
    
      let response = await tasque.getAllPosts();

      checkStatusCode(response, 200);
      checkResponseTime(response, 5000);
      checkResponseSchemas(response, schemas4);

      var createdPost = response.body.find(o => o.id === idPost);
  
      expect(createdPost.id).to.be.equal(idPost);
      expect(createdPost.previewImage).to.be.equal(previewImage);
      expect(createdPost.body).to.be.equal(postBody);
      expect(createdPost.author.id).to.be.equal(userId);
      expect(createdPost.author.userName).to.be.equal(userData.userName);
      expect(createdPost.author.email).to.be.equal(userData.email);
      expect(createdPost.reactions.length).to.be.equal(1); 
      expect(createdPost.comments.length).to.be.equal(1); 
      expect(createdPost.comments[0].id).to.be.equal(idComment);
      expect(createdPost.comments[0].body).to.be.equal(commentBody);
      expect(createdPost.reactions[0].isLike).to.be.equal(true);

      expect(createdPost.comments[0].author.email).to.be.equal(userData.email);
      expect(createdPost.comments[0].author.userName).to.be.equal(userData.userName);
      expect(createdPost.reactions[0].user.email).to.be.equal(userData.email);
      expect(createdPost.reactions[0].user.userName).to.be.equal(userData.userName);  
   });

    it(`update user for check user info under like and comment`, async () => {

        let response = await tasque.updateUser(token, userId, newUserData.avatar, newUserData.email, newUserData.userName);
       
        checkStatusCode(response, 204);
        checkResponseTime(response, 5000);
 
      });

    it(`check like and comment after update`, async () => {

        let response = await tasque.getAllPosts();
  
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkResponseSchemas(response, schemas4);
  
        var createdPost = response.body.find(o => o.id === idPost);
      
        expect(createdPost.id).to.be.equal(idPost);
        expect(createdPost.previewImage).to.be.equal(previewImage);
        expect(createdPost.body).to.be.equal(postBody);
        expect(createdPost.author.id).to.be.equal(userId);
        expect(createdPost.author.userName).to.be.equal(newUserData.userName);
        expect(createdPost.author.email).to.be.equal(newUserData.email);
        expect(createdPost.reactions.length).to.be.equal(1); 
        expect(createdPost.comments.length).to.be.equal(1); 
        expect(createdPost.comments[0].id).to.be.equal(idComment);
        expect(createdPost.comments[0].body).to.be.equal(commentBody);
        expect(createdPost.reactions[0].isLike).to.be.equal(true);
        expect(createdPost.comments[0].author.email).to.be.equal(newUserData.email);
        expect(createdPost.comments[0].author.userName).to.be.equal(newUserData.userName);
        expect(createdPost.reactions[0].user.email).to.be.equal(newUserData.email);
        expect(createdPost.reactions[0].user.userName).to.be.equal(newUserData.userName);
     });

     after('delete user', async () => {
      let response = await tasque.deleteUser(userId, token);

      checkStatusCode(response, 204);
      checkResponseTime(response, 5000);

    });

})